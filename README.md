# CHALLENGE VOX TECNOLOGIA BUILD A PARTNER COMPANY WEB SITE


**Prerequisites:** PHP, Node, [Okta developer account](https://developer.okta.com/)

> [Okta](https://developer.okta.com) has Authentication and User Management APIs that reduce development time with instant-on, scalable user infrastructure. Okta's intuitive API and expert support make it easy for developers to authenticate, manage, and secure users and roles in any application.

## Getting Started

Sign up for an [Okta developer account](https://developer.okta.com) and create a new application. Make note of the Client ID and Issuer values for your application.

Clone this project using the following commands:

```
git clone https://gitlab.com/johnoliver021/voxdesafio.git
cd voxdesafio
```

### Set up the Backend

Create the database POSTGRES and user for the project:

```
CREATE DATABASE partner_company
```

Copy and edit the `.env` file and enter the db details there:

```
cd partner-company-back-end
open .env file
edite the line 30 
DATABASE_URL=pgsql://postgres:123@127.0.0.1:5432/partner_company
to: DATABASE_URL=pgsql://<yourusername>:<yourpassword>@127.0.0.1:5432/partner_company
```

Edit `src/Controller/ApiController.php` and replace the Okta credentials with your own.

Install the project dependencies, run the migrations and then start the server:

```
composer install
php bin/console doctrine:migrations:migrate
php -S 0.0.0.0:8000 -t public
```

Loading [php -S 0.0.0.0:8000 -t public](http://localhost:8000/) now should show the default Symfony 4 page, and [http://localhost:8000/companies](localhost:8000/partners)
### Set up the Frontend

Edit the Okta configuration in `src/app/app.module.ts` to fill in your client ID and other application details. (Refer to the (Article Placeholder) for more details.) Then install the dependencies and run the server:

```
cd partner-company-front-end
npm install -g @angular/cli
npm install
ng serve --open
```

Loading [localhost:4200](localhost:4200) now should show you the application.

