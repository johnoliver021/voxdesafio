<?php
namespace App\Controller;

use App\Entity\Company;
use App\Entity\Partner;
use App\Repository\PartnerRepository;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PartnerController extends ApiController
{
    /**
    * @Route("/partners")
    * @Method("GET")
    */
    public function index(PartnerRepository $partnerRepository)
    {
        $partners = $partnerRepository->transformAll();

        return $this->respond($partners);
    }

    /**
    * @Route("/partners/{id}")
    * @Method("GET")
    */
    public function show($id, PartnerRepository $partnerRepository)
    {
        $partner = $partnerRepository->find($id);

        if(! $partner) {
            return $this->respondNotFound();
        }

        $partner = $partnerRepository->transform($partner);

        return $this->respond($partner);
    }

    

    /**
    * @Route("/partnerscreate")
    * @Method=("POST")
    */
    public function create(Request $request, PartnerRepository $partnerRepository, CompanyRepository $companyRepository, EntityManagerInterface $em)
    {
        
        
        $request = json_decode($request->getContent());

        if (! $request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        // validate the name
        if (! $request->name) {
            return $this->respondValidationError('Please provide a name!');
        }

        // validate the company
        if (! $request->company) {
            return $this->respondValidationError('Please provide a company!');
        }

        $company = $companyRepository->findOneByName($request->company);

        // persist the new partner
        $partner = new Partner;
        $partner->setName($request->name);
        $partner->setCompany($company);
        $em->persist($partner);
        $em->flush();

        return $this->respondCreated($partnerRepository->transform($partner));
    }
}