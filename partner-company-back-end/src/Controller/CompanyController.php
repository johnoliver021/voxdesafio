<?php
namespace App\Controller;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CompanyController extends ApiController
{
    /**
    * @Route("/companies")
    * @Method("GET")
    */
    public function index(CompanyRepository $companyRepository)
    {
        $companies = $companyRepository->transformAll();

        return $this->respond($companies);
    }

    /**
    * @Route("/companies/{id}")
    * @Method("GET")
    */
    public function show($id, CompanyRepository $companyRepository)
    {
        $company = $companyRepository->find($id);

        if(! $company) {
            return $this->respondNotFound();
        }

        $company = $companyRepository->transform($company);

        return $this->respond($company);
    }
    

    /**
    * @Route("/companiescreate")
    * @Method=("POST")
    */
    public function create(Request $request, CompanyRepository $companyRepository, EntityManagerInterface $em)
    {
        
        
        $request = json_decode($request->getContent());

        if (! $request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        // validate the name
        if (! $request->name) {
            return $this->respondValidationError('Please provide a name!');
        }

        //validate the address
        if (! $request->address) {
            return $this->respondValidationError('Please provide a address!');
        }

        // persist the new company
        $company = new Company;
        $company->setName($request->name);
        $company->setAddress($request->address);
        $em->persist($company);
        $em->flush();

        return $this->respondCreated($companyRepository->transform($company));
    }
}