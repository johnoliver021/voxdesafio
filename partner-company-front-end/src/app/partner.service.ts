import { Injectable } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

export interface Partner {
    id: Number,
    name: String,
    company: String,
    isUpdating: boolean
}

const API_URL: string = 'http://localhost:8000';

@Injectable({
    providedIn: 'root'
})
export class PartnerService {

    private accessToken;
    private headers;

    constructor(private oktaAuth: OktaAuthService, private http: Http) {
        this.init();
    }

    async init() {
        this.accessToken = await this.oktaAuth.getAccessToken();
        this.headers = new Headers({
            Authorization: 'Bearer ' + this.accessToken

        });
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded')
    }

    getPartners(): Observable<Partner[]> {
        return this.http.get(API_URL + '/partners',
            new RequestOptions({ headers: this.headers })
        )
            .map(res => {
                let modifiedResult = res.json();
                modifiedResult = modifiedResult.map(function (partner) {
                    partner.isUpdating = false;
                    return partner;
                });
                return modifiedResult;
            });
    }

    addPartner(partner): Observable<Partner> {
        return this.http.post(API_URL + '/partnerscreate', partner,
            new RequestOptions({ headers: this.headers })
        ).map(res => {
            return res.json();
        });
    }
}
