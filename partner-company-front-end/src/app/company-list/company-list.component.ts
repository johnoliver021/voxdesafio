import { Component, OnInit } from '@angular/core';
import { Company, CompanyService } from '../company.service';
import 'rxjs/Rx';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})

export class CompanyListComponent implements OnInit {

  companies: Company[];
  errorMessage: string;
  isLoading: boolean = true;

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
    this.getCompanies();
  }

  findCompany(id): Company {
    return this.companies.find(company => company.id === id);
  }

  isUpdating(id): boolean {
    return this.findCompany(id).isUpdating;
  }

  appendCompany(company: Company) {
    console.log(company.name)
    this.companies.push(company);
  }

  getCompanies() {
    this.companyService
      .getCompanies()
      .subscribe(
        companies => {
          this.companies = companies;
          this.isLoading = false;
        },
        error => this.errorMessage = <any>error
      );
  }

}