import { Component, OnInit } from '@angular/core';
import { Partner, PartnerService } from '../partner.service';
import 'rxjs/Rx';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.css']
})

export class PartnerListComponent implements OnInit {

  partners: Partner[];
  errorMessage: string;
  isLoading: boolean = true;

  constructor(private partnerService: PartnerService) { }

  ngOnInit() {
    this.getPartners();
  }

  findPartner(id): Partner {
    return this.partners.find(partner => partner.id === id);
  }

  isUpdating(id): boolean {
    return this.findPartner(id).isUpdating;
  }

  appendPartner(partner: Partner) {
    console.log(partner.name)
    this.partners.push(partner);
  }

  getPartners() {
    this.partnerService
      .getPartners()
      .subscribe(
        partners => {
          this.partners = partners;
          this.isLoading = false;
        },
        error => this.errorMessage = <any>error
      );
  }

}