import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { Company, CompanyService } from '../company.service';
import 'rxjs/Rx';
@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.css']
})
export class CompanyFormComponent implements OnInit {

  errors: string = '';
  isLoading: boolean = false;

  constructor(private companyService: CompanyService) { }

  @Output()
  companyAdded: EventEmitter<Company> = new EventEmitter<Company>();

  ngOnInit() {
  }

  addCompany(name, address) {
      
    this.isLoading = true;
    this.companyService
        .addCompany({
            name: name,
            address: address
        })
        .subscribe(
            company => {
                  
                this.isLoading = false;
                company.isUpdating = false;
                this.companyAdded.emit(company);
            },
            error => {
                this.errors = error.json().errors;
                console.log('e'+error)
                this.isLoading = false;
            }
        );
}

}
