import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { Partner, PartnerService } from '../partner.service';
import 'rxjs/Rx';
import { Company, CompanyService } from '../company.service';

@Component({
  selector: 'app-partner-form',
  templateUrl: './partner-form.component.html',
  styleUrls: ['./partner-form.component.css']
})
export class PartnerFormComponent implements OnInit {

  errors: string = '';
  isLoading: boolean = false;
  games : any;
  companies: any;
  errorMessage: string;


  constructor(private partnerService: PartnerService,private companyService: CompanyService ) {
      this.companies = this.getCompanies()
   }

  @Output()
  partnerAdded: EventEmitter<Partner> = new EventEmitter<Partner>();

  ngOnInit() {
  }
  getCompanies() {
    this.companyService
      .getCompanies()
      .subscribe(
        companies => {
          this.companies = companies;
          this.isLoading = false;
        },
        error => this.errorMessage = <any>error
      );
  }
  addPartner(name, company) {
      
    this.isLoading = true;
    this.partnerService
        .addPartner({
            name: name,
            company: company
        })
        .subscribe(
            partner => {
 
                this.isLoading = false;
                partner.isUpdating = false;
                this.partnerAdded.emit(partner);
            },
            error => {
                this.errors = error.json().errors;
                console.log('e'+error)
                this.isLoading = false;
            }
        );
}

}
