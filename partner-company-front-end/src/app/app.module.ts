import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { Routes, RouterModule } from '@angular/router';
import { OktaAuthModule, OktaCallbackComponent } from '@okta/okta-angular';
import { HttpModule } from '@angular/http';
import { CompanyFormComponent } from './company-form/company-form.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { PartnerFormComponent } from './partner-form/partner-form.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'companies', component: CompanyListComponent },
  { path: 'partners', component: PartnerListComponent },
  { path: 'implicit/callback', component: OktaCallbackComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];
const oktaConfig = {
  issuer: 'https://dev-861492.okta.com',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oa2c33uaoIkMRgAX357'
};
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CompanyListComponent,
    CompanyFormComponent,
    PartnerListComponent,
    PartnerFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    OktaAuthModule.initAuth(oktaConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
