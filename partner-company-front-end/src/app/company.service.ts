import { Injectable } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import{Response} from '@angular/http'

export interface Company {
    id: Number,
    name: String,
    address: String,
    isUpdating: boolean
}

const API_URL: string = 'http://localhost:8000';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    private accessToken;
    private headers;

    constructor(private oktaAuth: OktaAuthService, private http: Http) {
        this.init();
    }

    async init() {
        this.accessToken = await this.oktaAuth.getAccessToken();
        this.headers = new Headers({
            Authorization: 'Bearer ' + this.accessToken

        });
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded')
    }

    getCompanies(): Observable<Company[]> {
        return this.http.get(API_URL + '/companies',
            new RequestOptions({ headers: this.headers })
        )
            .map(res => {
                let modifiedResult = res.json();
                modifiedResult = modifiedResult.map(function (company) {
                    company.isUpdating = false;
                    return company;
                });
                return modifiedResult;
            });
    }

    addCompany(company): Observable<Company> {
        return this.http.post(API_URL + '/companiescreate', company,
            new RequestOptions({ headers: this.headers })
        ).map(res => {
            return res.json();
        });
    }
}
