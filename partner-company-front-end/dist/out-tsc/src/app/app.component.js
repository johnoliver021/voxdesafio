import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let AppComponent = class AppComponent {
    constructor(oktaAuth) {
        this.oktaAuth = oktaAuth;
        this.oktaAuth.$authenticationState.subscribe((isAuthenticated) => (this.isAuthenticated = isAuthenticated));
    }
    ngOnInit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.isAuthenticated = yield this.oktaAuth.isAuthenticated();
        });
    }
    login() {
        this.oktaAuth.loginRedirect('/');
    }
    logout() {
        this.oktaAuth.logout('/');
    }
};
AppComponent = tslib_1.__decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map