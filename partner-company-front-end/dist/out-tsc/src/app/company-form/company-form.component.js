import * as tslib_1 from "tslib";
import { Component, EventEmitter, Output } from '@angular/core';
import 'rxjs/Rx';
let CompanyFormComponent = class CompanyFormComponent {
    constructor(companyService) {
        this.companyService = companyService;
        this.errors = '';
        this.isLoading = false;
        this.companyAdded = new EventEmitter();
    }
    ngOnInit() {
    }
    addCompany(name, address) {
        this.isLoading = true;
        this.companyService
            .addCompany({
            name: name,
            address: address
        })
            .subscribe(company => {
            alert(company);
            this.isLoading = false;
            company.isUpdating = false;
            this.companyAdded.emit(company);
        }, error => {
            this.errors = error.json().errors;
            console.log('e' + error);
            this.isLoading = false;
        });
    }
};
tslib_1.__decorate([
    Output()
], CompanyFormComponent.prototype, "companyAdded", void 0);
CompanyFormComponent = tslib_1.__decorate([
    Component({
        selector: 'app-company-form',
        templateUrl: './company-form.component.html',
        styleUrls: ['./company-form.component.css']
    })
], CompanyFormComponent);
export { CompanyFormComponent };
//# sourceMappingURL=company-form.component.js.map