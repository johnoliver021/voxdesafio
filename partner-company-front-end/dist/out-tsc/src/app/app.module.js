import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { RouterModule } from '@angular/router';
import { OktaAuthModule, OktaCallbackComponent } from '@okta/okta-angular';
import { HttpClientModule } from '@angular/common/http';
import { CompanyFormComponent } from './company-form/company-form.component';
const routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'companies', component: CompanyListComponent },
    { path: 'implicit/callback', component: OktaCallbackComponent },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
const oktaConfig = {
    issuer: 'https://dev-861492.okta.com',
    redirectUri: 'http://localhost:4200/implicit/callback',
    clientId: '0oa2c33uaoIkMRgAX357'
};
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            HomeComponent,
            CompanyListComponent,
            CompanyFormComponent
        ],
        imports: [
            BrowserModule,
            HttpClientModule,
            AppRoutingModule,
            RouterModule.forRoot(routes),
            OktaAuthModule.initAuth(oktaConfig)
        ],
        providers: [],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map