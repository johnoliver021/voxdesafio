import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import 'rxjs/Rx';
let CompanyListComponent = class CompanyListComponent {
    constructor(companyService) {
        this.companyService = companyService;
        this.isLoading = true;
    }
    ngOnInit() {
        this.getCompanies();
    }
    findCompany(id) {
        return this.companies.find(company => company.id === id);
    }
    isUpdating(id) {
        return this.findCompany(id).isUpdating;
    }
    appendCompany(company) {
        console.log(company.name);
        this.companies.push(company);
    }
    getCompanies() {
        this.companyService
            .getCompanies()
            .subscribe(companies => {
            this.companies = companies;
            this.isLoading = false;
        }, error => this.errorMessage = error);
    }
};
CompanyListComponent = tslib_1.__decorate([
    Component({
        selector: 'app-company-list',
        templateUrl: './company-list.component.html',
        styleUrls: ['./company-list.component.css']
    })
], CompanyListComponent);
export { CompanyListComponent };
//# sourceMappingURL=company-list.component.js.map