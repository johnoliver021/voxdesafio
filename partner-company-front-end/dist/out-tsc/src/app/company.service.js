import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
const API_URL = 'http://localhost:8000';
let CompanyService = class CompanyService {
    constructor(oktaAuth, http) {
        this.oktaAuth = oktaAuth;
        this.http = http;
        this.init();
    }
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.accessToken = yield this.oktaAuth.getAccessToken();
            this.headers = new Headers({
                Authorization: 'Bearer ' + this.accessToken
            });
            this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
        });
    }
    getCompanies() {
        return this.http.get(API_URL + '/companies', new RequestOptions({ headers: this.headers }))
            .map(res => {
            let modifiedResult = res.json();
            modifiedResult = modifiedResult.map(function (company) {
                company.isUpdating = false;
                return company;
            });
            return modifiedResult;
        });
    }
    addCompany(company) {
        return this.http.post(API_URL + '/companies', JSON.stringify({ name: 'Adam Smith', address: 'aaa' }), new RequestOptions({ headers: this.headers })).map(res => {
            alert(res);
            return res.json();
        });
    }
};
CompanyService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], CompanyService);
export { CompanyService };
//# sourceMappingURL=company.service.js.map